package my.project.exeption;

public class ServerTransportException extends Exception{
    public ServerTransportException() {
    }

    public ServerTransportException(String message) {
        super(message);
    }

    public ServerTransportException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServerTransportException(Throwable cause) {
        super(cause);
    }
}
