package my.project.transport.impl.aio.client.handlers;

import my.project.transport.Connection;
import my.project.protocol.TransportProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;

public class ClientReadHandler implements CompletionHandler<Integer, ByteBuffer> {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private Connection connection;

    private String buffer = "";

    public ClientReadHandler(Connection connection) {
        this.connection = connection;
    }


    @Override
    public void completed(Integer bytesRead, ByteBuffer inputBuffer) {
        connection.onReadStart();
        try {
            LOGGER.debug("ClientReadHandler fired");
            if (bytesRead < 0) {
                connection.close();
                connection.onReadFinish();
                return;
            }

            if (bytesRead == 0 && buffer != null && !buffer.isEmpty()) {
                continueReading(inputBuffer);
                connection.onReadFinish();
                return;
            }
            inputBuffer.flip();
            String request = getRequest(bytesRead, inputBuffer);
            LOGGER.debug("ClientReadHandler read" + request);

            if (request.startsWith("{")) {
                if (request.contains("}")) {
                    Integer index = request.lastIndexOf('}');

                    String message = request.substring(0, index + 1);

                    if (index < request.length() - 1) {
                        buffer = request.substring(index + 1, request.length());
                        continueReading(inputBuffer);
                    } else {
                        buffer = "";
                    }

                    connection.addMessageToQueue(TransportProtocol.restoreMessage(message));
                    resetCondition(inputBuffer);
                } else {
                    buffer += request;
                    continueReading(inputBuffer);
                }
            } else {
                if (request.contains("}")) {
                    Integer index = request.lastIndexOf('}');

                    String message = request.substring(0, index + 1);
                    message = buffer + message;

                    if (index < request.length() - 1) {
                        buffer = request.substring(index + 1, request.length());
                        continueReading(inputBuffer);
                    } else {
                        buffer = "";
                    }


                    connection.addMessageToQueue(TransportProtocol.restoreMessage(message));
                    resetCondition(inputBuffer);

                } else {
                    buffer += request;
                    continueReading(inputBuffer);
                }
            }


        } catch (Exception e) {

        } finally {
            inputBuffer.compact();
            connection.onReadFinish();
        }
    }

    @Override
    public void failed(Throwable exc, ByteBuffer inputBuffer) {
        LOGGER.error("Read failed", exc);
        connection.onReadFinish();
    }

    private String getRequest(Integer bytesRead, ByteBuffer inputBuffer) {
        byte[] buffer = new byte[bytesRead];

        inputBuffer.get(buffer);
        return new String(buffer);
    }


    private void resetCondition(ByteBuffer inputBuffer) {
        buffer = "";
        inputBuffer.clear();
    }

    private void continueReading(ByteBuffer inputBuffer) {
        connection.onReadFinish();
        inputBuffer.clear();
        connection.read();
    }
}
