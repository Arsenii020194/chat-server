package my.project.transport;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

public interface AsyncSocketConnection {
    CompletionHandler<Integer, ByteBuffer> getOnRead();

    ByteBuffer getReadBuffer();

    CompletionHandler<Integer, ByteBuffer> getOnWrite();

    ByteBuffer getWriteBuffer();

    AsynchronousSocketChannel getChannel();

}
