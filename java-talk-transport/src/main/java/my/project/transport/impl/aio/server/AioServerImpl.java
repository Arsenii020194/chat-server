package my.project.transport.impl.aio.server;

import my.project.exeption.ServerTransportException;
import my.project.transport.Connection;
import my.project.transport.impl.aio.server.handlers.AcceptServerHandler;
import my.project.transport.server.ServerRequestManager;
import my.project.transport.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.ConcurrentHashMap;

public class AioServerImpl implements Server {
    private Logger LOGGER = LoggerFactory.getLogger(AioServerImpl.class);

    private final AsynchronousServerSocketChannel listener;
    private ConcurrentHashMap<Long, Connection> connectionsMap;

    private ServerRequestManager messageManager;

    private CompletionHandler handler;

    public AioServerImpl(AsynchronousServerSocketChannel listener, ServerRequestManager messageManager) {
        this.listener = listener;
        this.messageManager = messageManager;
        this.handler = new AcceptServerHandler(listener, this);
    }

    @Override
    public void start() {
        connectionsMap = new ConcurrentHashMap<>();
        listener.accept(null, handler);

    }

    @Override
    public void registerConnection(Connection connection) throws ServerTransportException {
        if (connection.getId() == null) {
            throw new ServerTransportException("Connection without identifier");
        }

        if (connectionsMap.get(connection.getId()) != null) {
            throw new ServerTransportException("Connection with id " + connection.getId() + " already registered");
        }

        connectionsMap.put(connection.getId(), connection);
    }

    @Override
    public void removeConnection(Connection connection) throws ServerTransportException {
        if (connectionsMap.get(connection.getId()) == null) {
            throw new ServerTransportException("Connection with id " + connection.getId() + " not registered");
        }

        connectionsMap.remove(connection.getId());
    }

    @Override
    public ServerRequestManager getMessageManager() {
        return messageManager;
    }
}
