package my.project.transport.impl.aio.server.handlers;

import my.project.transport.Connection;
import my.project.protocol.TransportProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;

public class ReadServerHandler implements CompletionHandler<Integer, ByteBuffer> {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private Connection connection;

    private String buffer = "";

    public ReadServerHandler(Connection connection) {
        this.connection = connection;
    }


    @Override
    public void completed(Integer bytesRead, ByteBuffer inputBuffer) {
        LOGGER.debug("ReadServerHandler fired");

        if (bytesRead < 0) {
            connection.close();
            return;
        }

        inputBuffer.flip();
        String request = getRequest(bytesRead, inputBuffer);

        LOGGER.debug("ReadServerHandler read " + request);

        if (request.startsWith("{")) {
            if (request.contains("}")) {
                Integer index = request.lastIndexOf('}');

                String message = request.substring(0, index + 1);

                if (index < request.length() - 1) {
                    buffer = request.substring(index + 1, request.length());
                } else {
                    buffer = "";
                }

            } else {
                buffer += request;
            }
        } else {
            if (request.contains("}")) {
                Integer index = request.lastIndexOf('}');

                String message = request.substring(0, index + 1);
                message = buffer + message;

                if (index < request.length() - 1) {
                    buffer = request.substring(index + 1, request.length());
                } else {
                    buffer = "";
                }


                connection.addMessageToQueue(TransportProtocol.restoreMessage(message));
                resetCondition(inputBuffer);

            } else {
                buffer += request;
            }
        }
        continueReading(inputBuffer);
        inputBuffer.compact();
    }

    @Override
    public void failed(Throwable exc, ByteBuffer inputBuffer) {
        LOGGER.error("Read failed", exc);
    }

    private String getRequest(Integer bytesRead, ByteBuffer inputBuffer) {
        return new String(getRequest(inputBuffer, bytesRead));
    }

    private byte[] getRequest(ByteBuffer inputBuffer, Integer bytesRead) {
        try {
            byte[] buffer = new byte[bytesRead];

            inputBuffer.get(buffer);

            inputBuffer.rewind();
            return buffer;
        } catch (Exception e){
            LOGGER.error("getRequest error", e);
        }

        return null;
    }

    private void resetCondition(ByteBuffer inputBuffer) {
//        buffer = "";
        inputBuffer.clear();
    }

    private void continueReading(ByteBuffer inputBuffer) {
        inputBuffer.clear();
        connection.read();
    }
}
