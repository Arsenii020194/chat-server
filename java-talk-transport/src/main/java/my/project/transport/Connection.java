package my.project.transport;

import my.project.exeption.ConnectionFailException;
import my.project.exeption.ConnectionNotRegisteredException;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

public interface Connection {
    void open() throws ConnectionFailException;

    Long getId();

    void close();

    void send(String message) throws IllegalArgumentException;

    void read();

    boolean isClosed();

    String getUserName();

    void setUserName(String userName);

    Boolean isRegistered();

    void addMessageToQueue(String request);

    void onReadFinish();

    void onReadStart();
}
