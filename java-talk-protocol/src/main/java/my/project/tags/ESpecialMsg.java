package my.project.tags;

public enum ESpecialMsg {
    HELP("help", HelpTagImpl.class, "возвращает список команд"),
    TEST("test", TestTagImpl.class, "возвращает тестовое сообщение");

    ESpecialMsg(String tag, Class<? extends SpecialMsg> clazz, String description) {
        this.tag = tag;
        this.clazz = clazz;
        this.description = description;
    }

    private String tag;
    private Class<? extends SpecialMsg> clazz;
    private String description;

    public static ESpecialMsg get(String tag) {
        if (tag == null || tag.isEmpty()) {
            return null;
        }

        for(ESpecialMsg eSpecialMsg : values()){
            if(eSpecialMsg.tag.equals(tag)){
                return eSpecialMsg;
            }
        }

        return null;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Class<? extends SpecialMsg> getClazz() {
        return clazz;
    }

    public void setClazz(Class<? extends SpecialMsg> clazz) {
        this.clazz = clazz;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
