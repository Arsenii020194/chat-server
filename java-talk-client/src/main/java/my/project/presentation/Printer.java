package my.project.presentation;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import my.project.protocol.Request;

public class Printer {

    private final static ColoredPrinter cp;

    static {
        cp = new ColoredPrinter.Builder(1, false).build();
    }

    public static void userMessage(Request request) {
        System.out.println(request.getUserName() + ": " + request.getBody());
//        cp.print(request.getUserName() + ": " + request.getBody(), Ansi.Attribute.DARK, Ansi.FColor.CYAN, Ansi.BColor.BLACK);
    }

    public static void systemMessage(String request) {
        System.out.println("** " + request + " **");
//        cp.println("** " + request + " **", Ansi.Attribute.DARK, Ansi.FColor.MAGENTA, Ansi.BColor.BLACK);
    }

    public static void errorMessage(String request) {
        System.out.println("** " + request + " **");
//        cp.println("** " + request + " **", Ansi.Attribute.DARK, Ansi.FColor.RED, Ansi.BColor.BLACK);
    }

    public static void simpleMessage(String request) {
        System.out.print(request);
//        cp.print(request, Ansi.Attribute.DARK, Ansi.FColor.YELLOW, Ansi.BColor.BLACK);
    }

    public static void loginMessage() {
//        cp.print("Введите логин для авторизации: ", Ansi.Attribute.DARK, Ansi.FColor.YELLOW, Ansi.BColor.BLACK);
        System.out.print("Введите логин для авторизации: ");
    }
}
