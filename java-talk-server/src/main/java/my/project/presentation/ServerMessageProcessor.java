package my.project.presentation;

import my.project.exeption.ExistLoginException;
import my.project.exeption.InvalidLoginException;
import my.project.exeption.MessageProcessorException;
import my.project.tags.ESpecialMsg;
import my.project.tags.SpecialMsg;
import my.project.transport.Connection;
import my.project.transport.server.ServerRequestManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import my.project.protocol.Request;
import my.project.protocol.Response;

import static com.google.common.base.Preconditions.checkArgument;

public class ServerMessageProcessor implements Runnable {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final Connection connection;
    private final ServerRequestManager requestManager;
    private final Request<String> request;

    public ServerMessageProcessor(Connection connection, ServerRequestManager requestManager, Request<String> request) {
        this.connection = connection;
        this.requestManager = requestManager;
        this.request = request;
    }

    @Override
    public void run() {
        try {
            _run();
        } catch (ExistLoginException | InvalidLoginException | MessageProcessorException e) {
            Response response = new Response(-1, e.getMessage());
            response.setId(request.getId());
            requestManager.onlyAuthorAnswer(response, connection);
            LOGGER.error("MessageProcessor error", e);
        }
    }

    private void _run() throws ExistLoginException, InvalidLoginException, MessageProcessorException {
        if (connection == null || request == null) {
            throw new MessageProcessorException();
        }

        if (connection.isClosed()) {
            requestManager.closeConnection(connection);
        }

        if (!connection.isRegistered()) {
            if (request.getUserName() == null || request.getUserName().isEmpty()) {
                throw new InvalidLoginException("Логин задан некорректно");
            }

            if (requestManager.getConnectionByName(request.getUserName()) != null) {
                throw new ExistLoginException("Логин занят: " + request.getUserName());
            }

            connection.setUserName(request.getUserName());

            requestManager.registerConnection(connection);
            Response response = new Response(requestManager.getLastRequests());
            response.setId(request.getId());

            requestManager.onlyAuthorAnswer(response, connection);
            return;
        }

        if (request.getBody() == null || request.getBody().isEmpty()) {
            throw new MessageProcessorException();
        }

        String specialMsg = checkAndGetSpecialMessage(request.getBody());
        if (specialMsg != null) {
            Response response = new Response(0, specialMsg);
            response.setId(request.getId());
            requestManager.onlyAuthorAnswer(response, connection);
            return;
        }

        Response response = new Response(0, "");
        response.setId(request.getId());
        requestManager.onlyAuthorAnswer(response, connection);
        requestManager.broadcast(request, connection);
    }

    private String checkAndGetSpecialMessage(String message) {
        if (message.contains(" ")) {
            return null;
        }

        ESpecialMsg eSpecialMsg = ESpecialMsg.get(request.getBody());

        if (eSpecialMsg != null) {
            try {
                SpecialMsg specialMsg = eSpecialMsg.getClazz().newInstance();
                return specialMsg.getAnswer();
            } catch (InstantiationException | IllegalAccessException e) {
                LOGGER.error("Get SpecialMsg class instance error", e);
            }
        }
        return null;
    }
}
