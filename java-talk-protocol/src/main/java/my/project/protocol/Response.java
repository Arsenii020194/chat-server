package my.project.protocol;

import java.util.List;

public class Response extends Request<List<Request>> {
    private Integer errorCode = 0;
    private String message;

    public Response() {
    }

    public Response(List<Request> body) {
        super(body);
    }

    public Response(Integer errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public Response(List<Request> body, Integer errorCode, String message) {
        super(body);
        this.errorCode = errorCode;
        this.message = message;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Response{" +
                "errorCode=" + errorCode +
                ", message='" + message + '\'' +
                '}';
    }
}

