package my.project.transport;

import my.project.exeption.ConnectionFailException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import static org.junit.jupiter.api.Assertions.assertThrows;

public abstract class ConnectionTest {

    public abstract Connection getBadConnection();

    @Test
    public void connection_should_be_registered_if_username_exist() {
        Connection connection = getBadConnection();
        assertFalse(connection.isRegistered());

        connection.setUserName("");
        assertFalse(connection.isRegistered());

        connection.setUserName("userName");
        assertTrue(connection.isRegistered());
    }

    @Test
    public void bad_connection_open_should_thrown_connectionFailException() {
        Throwable thrown = assertThrows(ConnectionFailException.class, () -> {
            getBadConnection().open();
        });
    }

    @Test
    public void connection_not_should_thrown_exception_if_not_open() {
        assertTrue(getBadConnection().isClosed());
    }

    @Test
    public void connection_send_empty_message_should_thrown_IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            getBadConnection().send("");
        });

        assertThrows(IllegalArgumentException.class, () -> {
            getBadConnection().send(null);
        });
    }

}
