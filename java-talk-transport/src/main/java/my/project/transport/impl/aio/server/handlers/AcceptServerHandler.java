package my.project.transport.impl.aio.server.handlers;

import my.project.exeption.ServerTransportException;
import my.project.transport.Connection;
import my.project.transport.server.Server;
import my.project.transport.impl.aio.server.AioServerConnectionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.atomic.AtomicLong;

public class AcceptServerHandler implements CompletionHandler<AsynchronousSocketChannel, Void> {
    private Logger LOGGER = LoggerFactory.getLogger(AcceptServerHandler.class);

    private final AsynchronousServerSocketChannel listener;
    private Server server;
    private AtomicLong connectionCount = new AtomicLong();


    public AcceptServerHandler(AsynchronousServerSocketChannel listener, Server server) {
        this.listener = listener;
        this.server = server;

    }

    @Override
    public void completed(AsynchronousSocketChannel socketChannel, Void sessionState) {
        LOGGER.debug("AcceptServerHandler fired");

        Connection connection = new AioServerConnectionImpl(socketChannel, connectionCount.incrementAndGet(), server.getMessageManager());

        try {
            connection.open();
            server.registerConnection(connection);
        } catch (ServerTransportException e) {
            LOGGER.error("Register connection error", e);
        } finally {
            listener.accept(null, this);
        }
    }

    @Override
    public void failed(Throwable exc, Void attachment) {
        LOGGER.error("Accept failed", exc);
    }
}
