package my.project.transport.impl.nio.client;

import com.google.common.base.Preconditions;
import my.project.protocol.TransportProtocol;
import my.project.transport.Connection;
import my.project.transport.Const;
import my.project.transport.NioSocketConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Reader implements Runnable {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private Connection connection;
    private String buffer = "";

    public Reader(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void run() {

        SocketChannel channel = ((NioSocketConnection) connection).getChannel();
        try {
            while (true) {
                String request = readRequest(channel);
                if (request == null) {
                    break;
                }

                if (request.contains("}")) {
                    Integer index = request.lastIndexOf('}');

                    String message = request.substring(0, index + 1);
                    message = buffer + message;

                    if (index < request.length() - 1) {
                        buffer = request.substring(index + 1, request.length());
                    } else {
                        buffer = "";
                    }


                    connection.addMessageToQueue(TransportProtocol.restoreMessage(message));

                    if (request.contains("{")) {
                        buffer = request.substring(request.lastIndexOf('{'));
                    } else {
                        buffer = "";
                        break;
                    }


                } else {
                    buffer += request;
                }
            }
        } catch (IllegalArgumentException e) {
            connection.close();
        } catch (Exception e) {
            LOGGER.error(buffer == null ? "" : buffer, e);
        }
    }

    public String readRequest(SocketChannel sc) throws Exception {

        ByteBuffer buffer = ByteBuffer.allocate(Const.bufferSize);
        int numRead = sc.read(buffer);

        Preconditions.checkArgument(numRead > -1);

        if (numRead == 0) {
            return "";
        }

        byte[] data = new byte[numRead];

        System.arraycopy(buffer.array(), 0, data, 0, numRead);
        return new String(data);
    }
}
