package my.project.presentation;

import my.project.protocol.Request;
import my.project.protocol.Response;
import my.project.protocol.TransportProtocol;
import my.project.transport.Connection;
import my.project.transport.client.ClientRequestManager;
import my.project.transport.impl.nio.client.Reader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class ClientRequestManagerImpl implements ClientRequestManager {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private Connection connection;

    private ConcurrentHashMap<Integer, Request> requests = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Integer, Response> responses = new ConcurrentHashMap<>();

    private ReentrantLock locker;
    private Condition condition;

    private AtomicInteger atomicInteger = new AtomicInteger(0);
    private ExecutorService executorService = Executors.newFixedThreadPool(1);

    private ScheduledExecutorService loopExecutorService = Executors.newScheduledThreadPool(1);

    public ClientRequestManagerImpl() {

    }

    public Response sendAndRead(Request request) throws NullPointerException, IllegalArgumentException {
        checkNotNull(request);
        checkNotNull(connection);

        Response response = null;

        try {
            request.setId(atomicInteger.incrementAndGet());
            requests.put(request.getId(), request);

            connection.send(TransportProtocol.unmarshall(request));

            locker.lock();
            while (responses.get(request.getId()) == null)
                condition.await();

            response = responses.get(request.getId());

        } catch (Exception e) {
            LOGGER.error("sendAndRead error", e);
        } finally {
            locker.unlock();
        }

        return response;
    }

    public void acceptResponse(Response response) throws NullPointerException, IllegalArgumentException {
        checkNotNull(response);
        checkArgument(response.getId() != null);

        LOGGER.debug("Accept response " + response);

        locker.lock();
        try {
            responses.put(response.getId(), response);
            condition.signal();
        } finally {
            locker.unlock();
        }
    }

    @Override
    public void close() {
        executorService.shutdown();
        loopExecutorService.shutdown();

        connection.close();
    }

    @Override
    public void init() {
        locker = new ReentrantLock();
        condition = locker.newCondition();

        loopExecutorService.scheduleAtFixedRate(new Reader(connection), 0, 5, TimeUnit.SECONDS);
    }

    public Response getResponse(Response response) {
        return responses.get(response.getId());
    }

    public Response getResponse(Integer id) {
        return responses.get(id);
    }

    public Condition getCondition() {
        return condition;
    }

    public ReentrantLock getLocker() {
        return locker;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
