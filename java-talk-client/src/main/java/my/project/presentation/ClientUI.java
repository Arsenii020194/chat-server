package my.project.presentation;

import my.project.exeption.ConnectionFailException;
import my.project.protocol.MessageImpl;
import my.project.protocol.Request;
import my.project.protocol.Response;
import my.project.transport.Configuration;
import my.project.transport.Connection;
import my.project.transport.impl.aio.client.AioClientConnectionImpl;
import my.project.transport.impl.nio.client.NioClientConnectionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class ClientUI {
    private Logger LOGGER = LoggerFactory.getLogger(AioClientConnectionImpl.class);

    private String host = Configuration.getServerAddress();
    private Integer port = Configuration.getServerPort();
    private Connection connection;
    private ClientRequestManagerImpl messageProcessor;

    private ScheduledExecutorService loopExecutorService = Executors.newScheduledThreadPool(1);

    public ClientUI() {
    }

    public ClientUI(String host, Integer port) {
        this.host = host;
        this.port = port;
    }

    public void start() {
        messageProcessor = new ClientRequestManagerImpl();
        connection = new NioClientConnectionImpl(new InetSocketAddress(host, port), messageProcessor);
        try {
            connection.open();
        }
        catch (ConnectionFailException e){
            LOGGER.error("Error connection", e);
            Printer.errorMessage("Не удалось подключиться к серверу");
            return;
        }

        messageProcessor.setConnection(connection);
        messageProcessor.init();

        boolean registered = false;
        while (!registered) {
            Printer.loginMessage();
            registered = login();
        }

        talk();
    }

    private void talk() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String msg = "";

        while (true) {

            try {
                Printer.simpleMessage("Вы: ");
                msg = br.readLine();

                if (msg.equals("exit")) {
                    loopExecutorService.shutdown();
                    messageProcessor.close();
                    break;
                }

                Request message = new MessageImpl(connection.getUserName(), msg);

                Response resp = messageProcessor.sendAndRead(message);

                if (resp.getErrorCode() == 0) {
                    Printer.systemMessage("delivered");

                    if (resp.getMessage() != null) {
                        System.out.println(resp.getMessage());
                    }

                    if (resp.getBody() != null) {
                        for (Request request : resp.getBody()) {
                            Printer.userMessage(request);
                        }
                    }

                } else {
                    Printer.errorMessage(resp.getMessage());
                }

            } catch (Exception e) {
                LOGGER.error("", e);
            }
        }
    }

    private boolean login() {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String login = null;
        try {
            login = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Request request = new MessageImpl(login, null);
        request.setUserName(login);
        try {
            Response resp = messageProcessor.sendAndRead(request);

            if (resp.getErrorCode() == 0) {
                connection.setUserName(login);
                Printer.systemMessage("Connection accept");

                if (resp.getBody() != null) {
                    for (Request req : resp.getBody()) {
                        Printer.userMessage(req);
                    }
                }

                return true;
            } else {
                Printer.errorMessage(resp.getMessage());
            }

        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return false;

    }
}
