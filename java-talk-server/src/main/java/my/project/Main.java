package my.project;

import my.project.presentation.RequestManagerImpl;
import my.project.protocol.Request;
import my.project.transport.Connection;
import my.project.transport.Const;
import my.project.transport.impl.nio.server.NioServerImpl;
import my.project.transport.server.ServerRequestManager;
import my.project.transport.server.Server;
import my.project.transport.impl.aio.server.AioServerImpl;
import my.project.transport.impl.aio.server.handlers.AcceptServerHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

public class Main {

    public static void main(String[] args) {
        try {

            ServerRequestManager messageManager = new RequestManagerImpl();
            Server nioServer = new NioServerImpl(messageManager);

            nioServer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startServer() throws IOException {

        AsynchronousChannelGroup channelGroup = AsynchronousChannelGroup.withFixedThreadPool(Runtime.getRuntime().availableProcessors(),
                Executors.defaultThreadFactory());
        AsynchronousServerSocketChannel listener = createListener(channelGroup);
        ConcurrentHashMap<String, Connection> connectionsMap = new ConcurrentHashMap<>();

        BlockingQueue<Request> requestQueue = new LinkedBlockingDeque<>();

        ServerRequestManager messageManager = new RequestManagerImpl();

        Server server = new AioServerImpl(listener, messageManager);

        server.start();


    }

    private static AsynchronousServerSocketChannel createListener(AsynchronousChannelGroup channelGroup) throws IOException {
        final AsynchronousServerSocketChannel listener = openChannel(channelGroup);
        listener.setOption(StandardSocketOptions.SO_REUSEADDR, true);
        listener.bind(new InetSocketAddress(Const.port));
        return listener;
    }

    private static AsynchronousServerSocketChannel openChannel(AsynchronousChannelGroup channelGroup) throws IOException {
        return AsynchronousServerSocketChannel.open(channelGroup);
    }

}
