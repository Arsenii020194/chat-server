package my.project.tags;

public class HelpTagImpl implements SpecialMsg {
    @Override
    public String getAnswer() {
        StringBuilder stringBuilder = new StringBuilder();

        for (ESpecialMsg eSpecialMsg : ESpecialMsg.values()){
            stringBuilder.append(eSpecialMsg.getTag()).append(" ").append(eSpecialMsg.getDescription()).append("\n");
        }
        return stringBuilder.toString();
    }
}
